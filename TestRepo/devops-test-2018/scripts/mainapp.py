import json

from fabric import tasks
from fabric.network import disconnect_all

from fabfile import get_version
from fabfile import vagrant


def main():
    vagrant()
    jsonobj = tasks.execute(get_version)
    json.loads(jsonobj)
    for x in jsonobj:
        print("%s: %d" % {x, jsonobj[x]})

    # Call this when you are done, or get an ugly exception!
    disconnect_all()


if __name__ == '__main__':
    main()
