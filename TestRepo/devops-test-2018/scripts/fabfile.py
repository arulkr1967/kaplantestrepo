import tempfile

import fabric.api
from fabric.api import get, run


def vagrant():

    """USAGE:
    fab vagrant uname

    Note that the command to run Fabric might be different on different
    platforms.
    """
    # change from the default user to 'vagrant'
    fabric.api.env.user = 'vagrant'
    # connect to the port-forwarded ssh
    fabric.api.env.hosts = ['127.0.0.1:2222']

    # find running VM (assuming only one is running)
    result = fabric.api.local('vagrant global-status | grep running', capture=True)
    machine_id = result.split()[0]

    # use vagrant ssh key for the running VM
    result = fabric.api.local('vagrant ssh-config {} | grep IdentityFile'.format(machine_id), capture=True)

    # env.key_filename = result.split()[1]
    # fabric.api.env.key_filename = result.split()[1]
    # work around as the above code not working due to vagrant version issue
    fabric.api.env.key_filename = "/home/arulkumar/.vagrant.d/boxes/kaplan-centos-7.0-64-puppet-3.8-preinstall/0/" \
                                  "virtualbox/vagrant_private_key"


def uname():
    run('uname -a')
    # run('vagrant version')


def get_version():
    uname()
    remote_path = '/var/www/devopstest/version'
    with tempfile.TemporaryFile() as fd:
        get(remote_path, fd)
        fd.seek(0)
        content = fd.read()
        return content
