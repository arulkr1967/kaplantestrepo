import unittest
import pep8
import subprocess

project_root = subprocess.check_output("git rev-parse --show-toplevel", shell=True).rstrip()

class TestCodeFormat(unittest.TestCase):

    def test_pep8_conformance(self):
        """Test to check conform to PEP8."""
        pep8style = pep8.StyleGuide(quiet=True)
        result = pep8style.check_files([project_root + 'TestRepo/devops-test-2018/scripts/mainapp.py'])
        print result.total_errors
        self.assertEqual(result.total_errors, 0,
                         "Found code style errors (and warnings).")

