class devops::apache(

  $package_name = 'httpd',

) {

  class { 'sudo': }
  sudo::conf { 'vagrant':
      priority => 60,
      content => "%sysadm ALL=(ALL) NOPASSWD: ALL",
  }

  file { "/etc/hostname":
    ensure  => present,
    owner   => root,
    group   => root,
    mode    => '0644',
    content => "kaplan-devops\n",
    notify  => Exec["set-hostname"],
  }

  exec { "set-hostname":
    command => '/bin/hostname -F /etc/hostname',
    unless  => "/usr/bin/test `hostname` = `/bin/cat /etc/hostname`",
  }

  package { $package_name:
    ensure  => installed,
  }

  service { 'httpd':
      ensure => running,
      enable => true,
      require => Package["httpd"],
   }

  file {'/etc/httpd/conf.d/testserver.conf':
      notify => Service["httpd"],
      ensure => file,
      require => [Package["httpd"],Exec['set-hostname']],
      content => template("devops/vhosts-rh.conf.erb"),
   }

   file { '/var/www/':
      ensure => "directory",
   }

   file { '/var/www/devopstest/':
      ensure => "directory",
      require => File['/var/www/'],
   }

  exec{'download_version':
      command => '/usr/bin/wget -q https://s3-eu-west-1.amazonaws.com/kaplandevopstest/version.json -O /var/www/devopstest/version.json',
      require => File['/var/www/devopstest/'],
    }

  file{'/var/www/devopstest/version.json':
      mode => 0755,
      require => Exec['download_version'],
    }

  file { '/var/www/devopstest/logs':
    ensure    => directory,
  }

}

